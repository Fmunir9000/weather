import acm.graphics.GCanvas;
import acm.graphics.GImage;
import acm.program.Program;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class GUI extends Program
{
    private JTextField inputField;
    private JLabel cityResult;
    private JLabel weatherResult;
    private JLabel tempResult;
    private GCanvas canvas;

    public GUI()
    {
        start();
    }

    public void init()
    {
        JLabel inputLabel = new JLabel("Zip Code");
        JLabel cityState = new JLabel("City, State:");
        JLabel weather = new JLabel("Weather:");
        JLabel temp = new JLabel("Temperature:");

        canvas = new GCanvas();
        this.add(canvas);

        canvas.add(inputLabel, 20, 20);
        canvas.add(cityState, 20, 50);
        canvas.add(weather, 20, 80);
        canvas.add(temp, 20, 110);

        inputField = new JTextField();
        inputField.setSize(300, 20);
        canvas.add(inputField, 100, 20);

        JButton goButton = new JButton("Go");
        canvas.add(goButton, 400, 20);

        JButton clearButton = new JButton("Clear");
        canvas.add(clearButton, 450, 20);

        cityResult = new JLabel();
        canvas.add(cityResult, 100, 50);

        weatherResult = new JLabel("   ");
        canvas.add(weatherResult, 100, 80);

        tempResult = new JLabel("   ");
        canvas.add(tempResult, 100, 110);

        addActionListeners();
    }

    public static void main(String[] args)
    {
        GUI g = new GUI();
    }

    public void actionPerformed(ActionEvent ae)
    {
        String whichButton = ae.getActionCommand();
        GImage iconImage = new GImage("");

        switch (whichButton)
        {
            case "Go":
                if (inputField.getText().length() > 0)
                {
                    Weather w = new Weather(inputField.getText());
                    cityResult.setText(w.getCityState());
                    weatherResult.setText(w.getWeather());
                    tempResult.setText(String.valueOf(w.getTemperature()));

                    cityResult.setSize(cityResult.getPreferredSize());
                    weatherResult.setSize(weatherResult.getPreferredSize());
                    tempResult.setSize(tempResult.getPreferredSize());

                    ImageIcon icon = new ImageIcon("images/" + w.getIcon() + ".gif");
                    iconImage = new GImage(icon.getImage());
                    canvas.add(iconImage, 200, 70);

                    iconImage.setVisible(true);
                }

                break;

            case "Clear":
                inputField.setText("");
                weatherResult.setText("");
                cityResult.setText("");
                tempResult.setText("");
                break;

        }
    }
}