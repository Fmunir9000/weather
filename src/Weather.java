import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;

public class Weather
{
    private final String ACCESS_TOKEN = "1655f919bbcd29ed";
    private JsonElement json;

    public Weather(String zipCode)
    {
        try
        {
            // Encode the user-supplied data to neutralize any special chars
            String encodedURL = URLEncoder.encode(zipCode, "utf-8");

            // Construct API URL
            String apiURL = "http://api.wunderground.com/api/" + ACCESS_TOKEN + "/conditions/q/"
                    + encodedURL + ".json";

            // Create URL object
            URL wundergroundURL = new URL(apiURL);

            // Create InputStream Object
            InputStream is = wundergroundURL.openStream();

            // Create InputStreamReader
            InputStreamReader isr = new InputStreamReader(is);

            // Parse input stream into a JsonElement
            JsonParser parser = new JsonParser();
            json = parser.parse(isr);
        }
        catch (java.net.MalformedURLException mue)
        {
            System.out.println("Malformed URL");
        }
        catch (java.io.IOException ioe)
        {
            System.out.println("IO Error");
        }
    }

    /**
     * Returns the current weather
     */
    public String getWeather()
    {
        return json.getAsJsonObject().get("current_observation").getAsJsonObject().get("weather").getAsString();
    }

    /**
     * Returns the fahrenheit temperature as a double
     */
    public double getTemperature()
    {
        return json.getAsJsonObject().get("current_observation").getAsJsonObject().get("temp_f").getAsDouble();
    }

    /**
     * Returns the city and state of the zip code
     */
    public String getCityState()
    {
        return json.getAsJsonObject().get("current_observation").getAsJsonObject().get("display_location")
                .getAsJsonObject().get("full").getAsString();
    }

    public String getIcon()
    {
        return json.getAsJsonObject().get("current_observation").getAsJsonObject().get("icon").getAsString();
    }

    public static void main(String[] args)
    {
        Weather w = new Weather("95677");

        String cityState = w.getCityState();
        String weatherCondition = w.getWeather();
        double temp = w.getTemperature();

        System.out.println(cityState);
        System.out.println(weatherCondition);
        System.out.println(temp);
    }
}